<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('payment_id');
            $table->bigInteger('item_id');
            $table->string('email');
            $table->string('country_name');
            $table->string('currency_code');
            $table->float('fee_amount');
            $table->float('taxamt');
            $table->timestamp('payment_date');
            $table->string('payment_method');
            $table->enum('payment_status',['success', 'failure'])->default('success');
            $table->timestamps();
        });

        Schema::table('payment_details', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
        
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_details');
    }
}

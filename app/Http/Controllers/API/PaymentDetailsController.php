<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\PaymentDetails;
use Validator;

class PaymentDetailsController extends Controller
{
    public function paymentdetails(Request $request)
    {
        try{
            $payement_details = PaymentDetails::where('user_id','=',Auth::user()->id)->get();
  
            if($payement_details->isEmpty()){
                $data['status'] = false;
                $data['status_code'] = 404;
                $data['message'] = 'Payment details data not found';  
            } else
            {
                $data['data'] = $payement_details; 
                $data['status'] = true;
                $data['status_code'] = 200;
                $data['message'] = 'Payment details retrieved';
            }     
          
        } catch (\Exception $ex){
            $data['status'] = false;
            $data['status_code'] = 500;
            $data['message'] = $ex->getMessage();  
        }
        return response()->json($data, 200);
    }

}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Validator;
use Hash;

use App\Mail\ForgotPassword;

use App\User;
use App\Product;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    
    public function register(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);
       
            if($validator->fails()){
                $data['status'] = false;
                $data['status_code'] = 404;
                $data['data'] = $validator->errors();
            }
    
            $input = $request->all();
            $user = User::where('email',$input['email']);
            // Email is already exists user
            if ($user->exists() > 0) {
                $data['status']   = false;
                $data['message']  = "Email already exists";
            } else {
                $input = $request->all();
                $input['password'] = Hash::make($input['password']);
                $user = User::create($input);
                $data['token'] =  $user->createToken('MyApp')->accessToken;

                $data['status'] = true;
                $data['status_code'] = '201';
                $data['message'] = 'User register successfully';
            }

          } catch (\Exception $e){
                $data['status'] = false;
                $data['status_code'] = 404;
                $data['message'] = $e->getMessage();
          }
    
          return response()->json($data, 200);
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
          try{
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required',
                ]);
        
                if ($validator->fails()) {          
                    $data['status'] = false;
                    $data['status_code'] = 404;
                    $data['data'] = $validator->errors();
                }  
                $request_data = $request->Input();

                $user = User::where('email' , $request_data['email']);

                if ($user->exists() > 0) {

                    $users = $user->get()->first();
                    if(Hash::check($request_data['password'], $users['password'])) {
                        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => '1'])){ 
                            $user = Auth::user(); 
                            $data['token'] =  $user->createToken('MyApp')-> accessToken; 
                            $data['first_name'] =  $user->first_name;
                            $data['last_name'] =  $user->last_name;
                            $data['message'] = 'User login successfully.';
                        } 
                        elseif(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => '0'])){
                            $data['status'] = false;
                            $data['status_code'] = 403;
                            $data['message'] = 'User denied';
                        }

                    } else {
                        $data['status']  = false;
                        $data['message'] = "Invalid Password";
                    }
                } else {
                    $data['status']  = false;
                    $data['message'] = "Invalid Email";
                }      
            } catch (\Exception $e){
                  $data['status'] = false;
                  $data['status_code'] = 404;
                  $data['message'] = $e->getMessage();
            }
            return response()->json($data, 200);
    }

    public function update_carddetails(Request $request){
        $result = [];
        try{
            $rules = array(
                'product_id'      => 'required',
                'user_id'         => 'required',
                'email'           => 'required|email',
                'last_opened_date'=> 'required',
            );

            $valid = Validator::make($request->all(), $rules);

            if ($valid->fails()) {

                $messages           = $valid->messages();
                $result['status']   = false;
                $result['messages'] = $messages;
                return response()->json($result);

            } else {
                $request_data = $request->Input();
                $users = User::where('email', $request_data['email']);

                if($users->count()>0){
                    $userdata = $users->get()->toArray();
                    $current_card = json_decode($userdata[0]['card_open']); 
                    $user_card = $request_data['product_id'];
                    if(empty($current_card) ){
                        $current_card = array();
                        array_push($current_card, (int)$user_card);
                        $users->update(['card_open' => $current_card, 'last_opened_date' => $request_data['last_opened_date']]);
                        $result['status'] = true ;
                        $result['message'] ="success";         
                    } else if(in_array($user_card, $current_card)){
                        $result['status']  = false;
                        $result['message'] = "card already exists";
                    } else {
                        array_push($current_card, (int)$request_data['product_id']);
                        $users->update(['card_open' => $current_card, 'last_opened_date' => $request_data['last_opened_date']]);
                        $result['status'] = true ;
                        $result['message'] ="success";
                    }
                } else {
                    $result['status']  = false;
                    $result['message'] = "Invalid Email";
                } 
            }

        } catch (\Exception $e){
                  $result['status'] = false;
                  $result['message'] = $e->getMessage();
        }
            return response()->json($result, 200);
    }

    public function viewprofile(Request $request)
    {   
        $result = [];
        try{
            $rules = array(
                'email'      => 'required|email'
            );

            $valid = Validator::make($request->all(), $rules);

            if ($valid->fails()) {

                $messages           = $valid->messages();
                $result['status']   = false;
                $result['messages'] = $messages;
                return response()->json($result);

            } else {
                $request_data = $request->Input();
                $userProfile = User::where('email', $request_data['email'])->get();

                if($userProfile->isEmpty()){
                    $result['status'] = false;
                    $result['status_code'] = 404;
                    $result['message'] = 'User profile details not found';  
                } else
                {
                    $result['status'] = true;
                    $result['status_code'] = 200;
                    $result['data'] = $userProfile; 
                    
                    $result['message'] = 'User profile details';
                }  
            }
        } catch (\Exception $e){
            $result['status'] = false;
            $result['status_code'] = 404;
            $result['message'] = $e->getMessage();
        }
    
        return response()->json($result, 200);
    }
    public function register_small(Request $request){
        $result = [];
        try{
            $rules = array(
                'email'      => 'required|email'
            );

            $valid = Validator::make($request->all(), $rules);

            if ($valid->fails()) {

                $messages           = $valid->messages();
                $result['status']   = false;
                $result['messages'] = $messages;
                return response()->json($result);

            } else {
                $request_data = $request->Input();

                $user = User::where('email',$request_data['email']);
                // Email is already exists user
                if ($user->exists() > 0) {
                    $result['status']   = false;
                    $result['message']  = "Email already exists";
                } else {
                    $request_data['first_name']     = (isset($request_data['first_name']) && !empty($request_data['first_name'])) ? $request_data['first_name'] : '';
                    $request_data['last_name']     = (isset($request_data['last_name']) && !empty($request_data['last_name'])) ? $request_data['last_name'] : '';
                    $request_data['password'] = '';

                    User::create($request_data);
                    $result['status'] = true ;
                    $result['message'] ="success";
                }
            }
        } catch (\Exception $e){
            $result['status'] = false;
            $result['status_code'] = 404;
            $result['message'] = $e->getMessage();
        }
    
        return response()->json($result, 200);    
    }
}

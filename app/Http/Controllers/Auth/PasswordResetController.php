<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Illuminate\Support\Str;
use Hash;

use App\User;
use App\PasswordReset;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $result = [];
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return response()->json([
                'status'=> false, 'message' => 'We canot find a user with that e-mail address.'
            ], 404);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(40)
             ]
        );
        if ($user && $passwordReset)
        {
            $url = url('/api/password/find/'.$passwordReset['token']);
            $mail = new PHPMailer;
            try{
                $mail->isSMTP();
                $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
                $mail->Port = 587;
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->SMTPAuth = true;
                $mail->Username = 'AKIA5DYMSLC5UUD2I3HR';
                $mail->Password = 'BA41GbI32VXzLVQ7/iwLcakhM36eKyxelvnrehnwUZcp';
                $mail->setFrom('no-reply@beingabundance.com', 'Being Abundance');
                $mail->addAddress($request->email);
                $mail->Subject = 'Password Reset Request';
                $mail->msgHTML("This is a test mail");
                $mail->msgHTML("<a href ='".$url."'>Reset Password</a>");
                if($mail->send()){
                    $result['status']  = true;
                    $result['message'] = "We have e-mailed your password reset link!";
                }else {
                    $result['status']  = false;
                    $result['message'] = "Enter valid crential";
                }
            } catch (\Exception $ex){
                $result['status'] = false;
                $result['status_code'] = 500;
                $result['message'] = $ex->getMessage();
            }
        }
        return response()->json($result, 200); 
    }
    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'status'=> false, 'message' => 'This password reset token is invalid.'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'status'=> false, 'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json($passwordReset);
    }
     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $result = [];
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required',
            'confirm_password' => 'required',
            'token' => 'required|string'
        ]);
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        if (!$passwordReset)
            return response()->json([
                'status'=> false, 'message' => 'This password reset token is invalid.'
            ], 404);
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->json([
                'status'=> false, 'message' => 'We canot find a user with that e-mail address.'
            ], 404);
        
        if($request->password != $request->confirm_password){
            return response()->json([
                'status'=> false, 'message' => "The password and confirm password doesn't match"
            ], 404);
        } else if($request->password == $request->confirm_password){

            $user->password = Hash::make($request->password);
            $user->save();
            $passwordReset->delete();
            $mail = new PHPMailer;
            try{
                $mail->isSMTP();
                $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
                $mail->Port = 587;
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->SMTPAuth = true;
                $mail->Username = 'AKIA5DYMSLC5UUD2I3HR';
                $mail->Password = 'BA41GbI32VXzLVQ7/iwLcakhM36eKyxelvnrehnwUZcp';
                $mail->setFrom('no-reply@beingabundance.com', 'Being Abundance');
                $mail->addAddress($request->email);
                $mail->Subject = 'Password Reset Success';
                $mail->msgHTML("You are changed your password successful.");
                if($mail->send()){
                    $result['status']  = true;
                    $result['message'] = "The password changed successful";
                }else {
                    $result['status']  = false;
                    $result['message'] = "Enter valid crential";
                }
            } catch (\Exception $ex){
                $result['status'] = false;
                $result['status_code'] = 500;
               
            }
        return response()->json($result, 200); 
        }
    }
}